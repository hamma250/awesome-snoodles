package org.gameroo.awesomesnoodle;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.PooledEngine;
import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;
import org.gameroo.awesomesnoodle.components.*;
import org.gameroo.awesomesnoodle.systems.*;

public class GameScreen extends ScreenAdapter {
    static final int GAME_READY = 0;
    static final int GAME_RUNNING = 1;
    static final int GAME_PAUSED = 2;
    static final int GAME_LEVEL_END = 3;
    static final int GAME_OVER = 4;

    MainGameClass game;

    OrthographicCamera guiCam;
    Vector3 touchPoint;
    //World world;
    //CollisionListener collisionListener;
    Rectangle pauseBounds;
    Rectangle resumeBounds;
    Rectangle quitBounds;
    int lastScore;
    String scoreString;

    PooledEngine engine;
    private GlyphLayout layout = new GlyphLayout();

    private int state;

    private Entity Snake;

    public GameScreen(MainGameClass game){
        this.game = game;
        Gdx.app.setLogLevel(Application.LOG_DEBUG);
        state = GAME_READY;
        guiCam = new OrthographicCamera(MainGameClass.gameScreenWidth, MainGameClass.gameScreenHeight);
        guiCam.position.set(MainGameClass.gameScreenWidth / 2, MainGameClass.gameScreenHeight / 2, 0);
        touchPoint = new Vector3();

        engine = new PooledEngine();

        Snake = createSnakeHead();

        engine.addSystem(new SnakeSystem());
        engine.addSystem(new BoundsSystem());
        //engine.addSystem(new FollowSnakeSystem());
        engine.addSystem(new StateSystem());
        engine.addSystem(new RenderingSystem(game.batch));
    }

    private Entity createSnakeHead() {
        Entity entity = engine.createEntity();

        SnakeComponent snake = engine.createComponent(SnakeComponent.class);
        BoundsComponent bounds = engine.createComponent(BoundsComponent.class);
        VelocityComponent movement = engine.createComponent(VelocityComponent.class);
        PositionComponent position = engine.createComponent(PositionComponent.class);
        StateComponent state = engine.createComponent(StateComponent.class);
        TextureComponent texture = engine.createComponent(TextureComponent.class);

        bounds.bounds.width = SnakeComponent.WIDTH;
        bounds.bounds.height = SnakeComponent.HEIGHT;

        position.pos.set(0f, 0f, 0f);

        Texture tes = new Texture("snake.png");
        texture.region = new TextureRegion(tes,0,0,192,192);

        state.set(SnakeComponent.STATE_UP);

        entity.add(snake);
        entity.add(bounds);
        entity.add(movement);
        entity.add(position);
        entity.add(state);
        entity.add(texture);

        engine.addEntity(entity);
        return entity;
    }

   /* private void createCamera(Entity target) {
        Entity entity = engine.createEntity();

        CameraComponent camera = new CameraComponent();
        camera.camera = engine.getSystem(RenderingSystem.class).getCamera();
        camera.target = target;

        entity.add(camera);

        engine.addEntity(entity);
    }*/

    @Override
    public void render(float delta) {
        super.render(delta);
        update(delta);
    }

    public void update (float deltaTime) {
        if (deltaTime > 0.1f) deltaTime = 0.1f;

        engine.update(deltaTime);

        switch (state) {
            case GAME_READY:
                updateReady(deltaTime);
                break;
            case GAME_RUNNING:
                updateRunning(deltaTime);
                break;
            case GAME_PAUSED:
                //updatePaused();
                break;
            case GAME_LEVEL_END:
                //updateLevelEnd();
                break;
            case GAME_OVER:
                //updateGameOver();
                break;
        }
    }

    private void updateReady(float deltaTime){
        if (Gdx.input.isKeyPressed(Input.Keys.SPACE)) state = GAME_RUNNING;
    }

    private void updateRunning (float deltaTime) {
        /*if (Gdx.input.justTouched()) {
            guiCam.unproject(touchPoint.set(Gdx.input.getX(), Gdx.input.getY(), 0));

            if (pauseBounds.contains(touchPoint.x, touchPoint.y)) {
                //Assets.playSound(Assets.clickSound);
                state = GAME_PAUSED;
                //pauseSystems();
                return;
            }
        }*/

        //Application.ApplicationType appType = Gdx.app.getType();

        // should work also with Gdx.input.isPeripheralAvailable(Peripheral.Accelerometer)
        if(Gdx.input.isKeyPressed(Input.Keys.DPAD_LEFT)){
            engine.getSystem(SnakeSystem.class).setStateChanged(SnakeComponent.STATE_LEFT,Snake);
        }
        if(Gdx.input.isKeyPressed(Input.Keys.DPAD_RIGHT)){
            engine.getSystem(SnakeSystem.class).setStateChanged(SnakeComponent.STATE_RIGHT,Snake);
        }
        if(Gdx.input.isKeyPressed(Input.Keys.DPAD_UP)){
            engine.getSystem(SnakeSystem.class).setStateChanged(SnakeComponent.STATE_UP,Snake);
        }
        if(Gdx.input.isKeyPressed(Input.Keys.DPAD_DOWN)){
            engine.getSystem(SnakeSystem.class).setStateChanged(SnakeComponent.STATE_DOWN,Snake);
        }
    }
}
