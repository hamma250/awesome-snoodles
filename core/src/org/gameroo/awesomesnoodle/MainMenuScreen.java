package org.gameroo.awesomesnoodle;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;

public class MainMenuScreen extends ScreenAdapter {
    MainGameClass game;
    OrthographicCamera guiCam;
    Rectangle playBounds;
    Rectangle highscoresBounds;
    Vector3 touchPoint;

    public MainMenuScreen (MainGameClass game) {
        this.game = game;

        guiCam = new OrthographicCamera(MainGameClass.gameScreenWidth, MainGameClass.gameScreenHeight);
        guiCam.position.set(MainGameClass.gameScreenWidth / 2, MainGameClass.gameScreenHeight / 2, 0);
        playBounds = new Rectangle(25, 350, 300, 50);
        highscoresBounds = new Rectangle(160 - 150, 200 - 18, 300, 36);
        touchPoint = new Vector3();
    }

    public void update () {
        if (Gdx.input.justTouched()) {
            guiCam.unproject(touchPoint.set(Gdx.input.getX(), Gdx.input.getY(), 0));

            if (playBounds.contains(touchPoint.x, touchPoint.y)) {
                game.setScreen(new GameScreen(game));
                return;
            }

            if (highscoresBounds.contains(touchPoint.x, touchPoint.y)) {
                //game.setScreen(new HighscoresScreen(game));
                return;
            }
        }
    }

    public void draw () {
        GL20 gl = Gdx.gl;
        gl.glClearColor(1, 0, 0, 1);
        gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        guiCam.update();
        game.batch.setProjectionMatrix(guiCam.combined);

        game.batch.disableBlending();
        game.batch.begin();
        //game.batch.draw(Assets.backgroundRegion, 0, 0, gameScreenWidht, gameScreenHeight);
        game.batch.end();

        game.batch.enableBlending();
        game.batch.begin();
        game.batch.draw(new Texture("button_play.png"), 25, 350, 300, 50);
        game.batch.end();
    }

    @Override
    public void render (float delta) {
        update();
        draw();
    }

    @Override
    public void pause () {
    }
}
