package org.gameroo.awesomesnoodle.components;

import com.badlogic.ashley.core.Component;

public class SnakeComponent implements Component {
        public static final int STATE_HIT = 0;
        public static final int STATE_UP = 1;
        public static final int STATE_DOWN = 2;
        public static final int STATE_LEFT = 3;
        public static final int STATE_RIGHT = 4;
        public static final int STATE_ZERO = 5;
        public static final float WIDTH = 0.8f;
        public static final float HEIGHT = 0.8f;

        public int stateOld = SnakeComponent.STATE_UP;
        public int stateNew = SnakeComponent.STATE_UP;
        public boolean stateChanged = false;

        public boolean readyToTurn = false;
        public float oldPos = 0f;

        public static final int snakeWidth = 192;
        public static final int snakeHeight = 192;

        public float speed = 1.5f;
        public int lengthSoFar = 0;
}
