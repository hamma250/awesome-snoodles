package org.gameroo.awesomesnoodle.systems;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import org.gameroo.awesomesnoodle.components.BoundsComponent;
import org.gameroo.awesomesnoodle.components.PositionComponent;

public class BoundsSystem extends IteratingSystem {

    private ComponentMapper<PositionComponent> tm;
    private ComponentMapper<BoundsComponent> bm;

    public BoundsSystem() {
        super(Family.all(BoundsComponent.class, PositionComponent.class).get());

        tm = ComponentMapper.getFor(PositionComponent.class);
        bm = ComponentMapper.getFor(BoundsComponent.class);
    }

    @Override
    public void processEntity(Entity entity, float deltaTime) {
        PositionComponent pos = tm.get(entity);
        BoundsComponent bounds = bm.get(entity);

        bounds.bounds.x = pos.pos.x - bounds.bounds.width * 0.5f;
        bounds.bounds.y = pos.pos.y - bounds.bounds.height * 0.5f;
    }
}