package org.gameroo.awesomesnoodle.systems;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.badlogic.gdx.Gdx;
import org.gameroo.awesomesnoodle.MainGameClass;
import org.gameroo.awesomesnoodle.components.PositionComponent;
import org.gameroo.awesomesnoodle.components.SnakeComponent;
import org.gameroo.awesomesnoodle.components.StateComponent;
import org.gameroo.awesomesnoodle.components.VelocityComponent;

public class SnakeSystem extends IteratingSystem {
    protected static final Family family = Family.all(SnakeComponent.class,
            StateComponent.class,
            PositionComponent.class,
            VelocityComponent.class).get();

    protected final ComponentMapper<SnakeComponent> sm;
    protected final ComponentMapper<StateComponent> StateM;
    protected final ComponentMapper<PositionComponent> pm;
    protected final ComponentMapper<VelocityComponent> cm;

    public SnakeSystem() {
        super(family);

        sm = ComponentMapper.getFor(SnakeComponent.class);
        StateM = ComponentMapper.getFor(StateComponent.class);
        pm = ComponentMapper.getFor(PositionComponent.class);
        cm = ComponentMapper.getFor(VelocityComponent.class);
    }

    public void setStateChanged(int state, Entity entity){
        SnakeComponent snake = sm.get(entity);
        snake.stateNew = state;
        //TODO: kann man entfernen. einfach checken ob old und new state gleich sind
        snake.stateChanged = true;
    }

    @Override
    public void update(float deltaTime) {
        super.update(deltaTime);
    }

    @Override
    public void processEntity(Entity entity, float deltaTime) {
        PositionComponent pos = pm.get(entity);
        StateComponent state = StateM.get(entity);
        VelocityComponent mov = cm.get(entity);
        SnakeComponent snake = sm.get(entity);

        int stateNew = snake.stateNew;
        int stateOld = snake.stateOld;

        //process movement direction
        if(snake.stateChanged && snake.readyToTurn){
            if(stateNew == SnakeComponent.STATE_RIGHT && stateOld != SnakeComponent.STATE_LEFT){
                state.set(SnakeComponent.STATE_RIGHT);
                snake.stateOld = stateNew;
            }
            if(stateNew == SnakeComponent.STATE_LEFT && stateOld != SnakeComponent.STATE_RIGHT){
                state.set(SnakeComponent.STATE_LEFT);
                snake.stateOld = stateNew;
            }
            if(stateNew == SnakeComponent.STATE_UP && stateOld != SnakeComponent.STATE_DOWN){
                state.set(SnakeComponent.STATE_UP);
                snake.stateOld = stateNew;
            }
            if(stateNew == SnakeComponent.STATE_DOWN && stateOld != SnakeComponent.STATE_UP){
                state.set(SnakeComponent.STATE_DOWN);
                snake.stateOld = stateNew;
            }
            snake.stateChanged = false;
            snake.readyToTurn = false;
            snake.oldPos = 0f;
        }

        float posToMov = SnakeComponent.snakeWidth *deltaTime*snake.speed;
        // Check for state
        if(state.get() == SnakeComponent.STATE_RIGHT){
            pos.pos.x += posToMov;
        }

        if(state.get() == SnakeComponent.STATE_LEFT){
            pos.pos.x -= posToMov;
        }

        if(state.get() == SnakeComponent.STATE_UP){
            pos.pos.y += posToMov;
        }

        if(state.get() == SnakeComponent.STATE_DOWN){
            pos.pos.y -= posToMov;
        }

        // infinity screen
        if (pos.pos.x < 0) {
            pos.pos.x = MainGameClass.gameScreenWidth;
        }

        if (pos.pos.x > MainGameClass.gameScreenWidth) {
            pos.pos.x = 0;
        }

        if (pos.pos.y < 0) {
            pos.pos.y = MainGameClass.gameScreenHeight;
        }

        if (pos.pos.y > MainGameClass.gameScreenHeight) {
            pos.pos.y = 0;
        }

        float localPosition = snake.oldPos * (1/RenderingSystem.PIXELS_TO_METRES);
        if(localPosition >= SnakeComponent.snakeWidth){
            //TODO: reset pos so there won't be any greater deviations from the grid
            /*float rest = localPosition % SnakeComponent.snakeWidth;
            if(rest >= SnakeComponent.snakeWidth){
                pos.pos.x = pos.pos.x + SnakeComponent.snakeWidth-rest;
            }else{
                pos.pos.x = pos.pos.x - rest;
            }*/
            snake.readyToTurn = true;
            snake.oldPos = 0f;
        }else{
            snake.oldPos += posToMov;
            snake.readyToTurn = false;
        }

        // TODO: add counter
        snake.lengthSoFar = 0;

        //TODO: Check if hit a tail with this head

    }

}

