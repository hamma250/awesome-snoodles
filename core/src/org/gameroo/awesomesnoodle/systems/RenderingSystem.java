package org.gameroo.awesomesnoodle.systems;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.Array;
import org.gameroo.awesomesnoodle.components.PositionComponent;
import org.gameroo.awesomesnoodle.components.TextureComponent;

import static org.gameroo.awesomesnoodle.MainGameClass.gameScreenHeight;
import static org.gameroo.awesomesnoodle.MainGameClass.gameScreenWidth;

public class RenderingSystem extends IteratingSystem {
    static final float FRUSTUM_WIDTH = gameScreenWidth;
    static final float FRUSTUM_HEIGHT = gameScreenHeight;
    static final float PIXELS_TO_METRES = 1.0f / 8.0f;

    private SpriteBatch batch;
    private Array<Entity> renderQueue;
    private OrthographicCamera cam;

    private ComponentMapper<TextureComponent> textureM;
    private ComponentMapper<PositionComponent> transformM;

    public RenderingSystem(SpriteBatch batch) {
        super(Family.all(PositionComponent.class, TextureComponent.class).get());

        textureM = ComponentMapper.getFor(TextureComponent.class);
        transformM = ComponentMapper.getFor(PositionComponent.class);

        renderQueue = new Array<Entity>();

        this.batch = batch;

        cam = new OrthographicCamera(FRUSTUM_WIDTH, FRUSTUM_HEIGHT);
        cam.position.set(FRUSTUM_WIDTH / 2, FRUSTUM_HEIGHT / 2, 0);
    }

    @Override
    public void update(float deltaTime) {
        super.update(deltaTime);

        cam.update();
        batch.setProjectionMatrix(cam.combined);
        batch.begin();

        for (Entity entity : renderQueue) {
            TextureComponent tex = textureM.get(entity);

            if (tex.region == null) {
                continue;
            }

            PositionComponent t = transformM.get(entity);

            float width = tex.region.getRegionWidth();
            float height = tex.region.getRegionHeight();
            float originX = 0f;
            float originY = 0f;

            batch.draw(tex.region,
                    t.pos.x, t.pos.y,
                    originX,originY,
                    width, height,
                    t.scale.x * PIXELS_TO_METRES, t.scale.y * PIXELS_TO_METRES,
                    MathUtils.radiansToDegrees * t.rotation);
        }

        batch.end();
        renderQueue.clear();
    }

    @Override
    public void processEntity(Entity entity, float deltaTime) {
        renderQueue.add(entity);
    }

    public OrthographicCamera getCamera() {
        return cam;
    }
}